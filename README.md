# README #

Welcome to the EverSector repository on Bitbucket! EverSector is a space-themed roguelike written in Java, using AsciiPanel for its display.

### Builds ###

After new features are added to the game, I will run a Bitbucket Pipelines build. The most recent build will always be posted to Downloads.

No sound files are included for the sake of file size. If you want to play with sound effects, download the audio.zip file from Downloads and unzip it to your assets folder. If you use Windows and don't have Java installed, you can download the windows.zip file and unzip it to your EverSector directory. This contains an executable launcher and a bundled JRE.

Keep in mind that these builds probably won't be as stable as the main releases posted on itch.io. If you find a bug, please post it on the issue tracker.

### Contributing ###

Though EverSector does not rely on contributions, I will gladly accept reasonable pull requests. EverSector's issues and planned features are listed on the [Trello board](https://trello.com/b/nOsMSRe3), which can also be viewed from the Boards tab. If you are interested in contributing, I would recommend joining the [Discord server](https://discord.gg/TZTEQTz) to discuss changes. Notable contributors will be credited in the game's README and receive a Contributor role on Discord.

Make sure all contributions are documented with JavaDoc comments where possible. If you have IntelliJ IDEA, please download the EverSector.xml code style from Downloads and apply it to files you change (default is Ctrl+Alt+L).

### Links ###

* [itch.io Page](https://boldorf.itch.io/eversector)
* [Trello Board](https://trello.com/b/nOsMSRe3)
* [Subreddit](https://reddit.com/r/EverSector)
* [Discord Server](https://discord.gg/TZTEQTz)
* [Twitter](https://twitter.com/DLMogrithe)
* [YouTube Channel](https://youtube.com/BoldorfSmokebane)
* [RogueBasin Page](http://roguebasin.com/index.php?title=EverSector)