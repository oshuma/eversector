/**
 * The actions that can be performed by ships.
 *
 * @author Boldorf Smokebane
 */
package boldorf.eversector.actions;