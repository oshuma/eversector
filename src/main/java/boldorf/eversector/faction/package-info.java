/**
 * Information regarding factions and their relationships.
 *
 * @author Boldorf Smokebane
 */
package boldorf.eversector.faction;