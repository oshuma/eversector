/**
 * Ships and abstract concepts surrounding them.
 *
 * @author Boldorf Smokebane
 */
package boldorf.eversector.ships;