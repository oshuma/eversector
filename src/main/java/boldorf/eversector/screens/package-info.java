/**
 * All screens passed to Display.
 *
 * @author Boldorf Smokebane
 */
package boldorf.eversector.screens;