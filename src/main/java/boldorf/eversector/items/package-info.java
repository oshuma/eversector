/**
 * Different types of items and resources that can be purchased and used by ships.
 *
 * @author Boldorf Smokebane
 */
package boldorf.eversector.items;