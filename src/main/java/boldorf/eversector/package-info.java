/**
 * Core components of the game that do not fit into other packages.
 *
 * @author Boldorf Smokebane
 */
package boldorf.eversector;