/**
 * All components on the map, such as galaxies, sectors, and planets.
 *
 * @author Boldorf Smokebane
 */
package boldorf.eversector.map;